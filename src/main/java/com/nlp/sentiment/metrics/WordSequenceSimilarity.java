package com.nlp.sentiment.metrics;

public class WordSequenceSimilarity implements Similarity<String> {

	private final double sigma;

	public WordSequenceSimilarity(double sigma) {
		this.sigma = sigma;
	}

	public double similarityScore(String u, String v) {
		WordSequenceDistance distance = new WordSequenceDistance();
		double ddistance = distance.getDistance(u, v);
		return Math.exp(ddistance * ddistance * sigma);
	}

}
