package com.nlp.sentiment.metrics;

import static com.nlp.sentiment.utils.ContentFeatureExtractor.getFeatureMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;

import com.aliasi.matrix.SparseFloatVector;
import com.aliasi.matrix.Vector;

public class CosineDistance implements Distance<String> {

	public double getDistance(String str1, String str2) {

		Map<String, ? extends Number> featureMap1 = getFeatureMap(str1);
		Map<String, ? extends Number> featureMap2 = getFeatureMap(str2);
		Set<String> totalFeatures = new HashSet<String>();

		if (!MapUtils.isEmpty(featureMap1)) {
			totalFeatures.addAll(featureMap1.keySet());
		}
		if (!MapUtils.isEmpty(featureMap2)) {
			totalFeatures.addAll(featureMap2.keySet());
		}

		float[] arrayOfFeature1 = new float[totalFeatures.size()];
		float[] arrayOfFeature2 = new float[totalFeatures.size()];

		int acc = 0, arrayKey[] = new int[totalFeatures.size()];

		for (String feature : totalFeatures) {
			arrayKey[acc] = acc;
			int count1 = 0, count2 = 0;
			if (featureMap1 != null && featureMap1.containsKey(feature)) {
				count1 = featureMap1.get(feature).intValue();
			}
			if (featureMap2 != null && featureMap2.containsKey(feature)) {
				count2 = featureMap2.get(feature).intValue();
			}
			arrayOfFeature1[acc] = count1;
			arrayOfFeature2[acc] = count2;
			acc++;
		}
		Vector vector1 = new SparseFloatVector(arrayKey, arrayOfFeature1,
				totalFeatures.size());
		Vector vector2 = new SparseFloatVector(arrayKey, arrayOfFeature2,
				totalFeatures.size());
		com.aliasi.matrix.CosineDistance cd = new com.aliasi.matrix.CosineDistance();
		return cd.distance(vector1, vector2);
	}

}
