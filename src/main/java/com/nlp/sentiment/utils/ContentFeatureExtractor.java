package com.nlp.sentiment.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.aliasi.tokenizer.EnglishStopTokenizerFactory;
import com.aliasi.tokenizer.LowerCaseTokenizerFactory;
import com.aliasi.tokenizer.NGramTokenizerFactory;
import com.aliasi.tokenizer.RegExFilteredTokenizerFactory;
import com.aliasi.tokenizer.TokenFeatureExtractor;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.FeatureExtractor;
import com.aliasi.util.Pair;

public class ContentFeatureExtractor {

	private static final Pair<Integer, Integer> NGRAM_LENGTH = new Pair<Integer, Integer>(
			4, 6);
	private static FeatureExtractor<CharSequence> featureExtractor = null;
	private static final String wordToken = "\\s?\\S+\\s?";
	private static final Pattern pattern = Pattern.compile(wordToken);

	public static Map<String, ? extends Number> getFeatureMap(String content) {

		TokenizerFactory ngramFactory = new NGramTokenizerFactory(
				NGRAM_LENGTH.a(), NGRAM_LENGTH.b());
		ngramFactory = new LowerCaseTokenizerFactory(ngramFactory);
		ngramFactory = new EnglishStopTokenizerFactory(ngramFactory);
		ngramFactory = new RegExFilteredTokenizerFactory(ngramFactory, pattern);

		Map<String, ? extends Number> featureMap = null;
		if (featureExtractor == null) {
			featureExtractor = (FeatureExtractor<CharSequence>) new TokenFeatureExtractor(
					ngramFactory);
		} else {
			featureMap = featureExtractor.features(content);
		}
		return featureMap;
	}

	public static List<String> getFeatures(String content) {
		Map<String, ? extends Number> featureMap = getFeatureMap(content);
		return new ArrayList<String>(featureMap.keySet());
	}

	public static List<Float> getFeatureValues(String content) {
		@SuppressWarnings("unchecked")
		Map<String, Float> featureMap = (Map<String, Float>) getFeatureMap(content);
		return new ArrayList<Float>(featureMap.values());
	}
}
