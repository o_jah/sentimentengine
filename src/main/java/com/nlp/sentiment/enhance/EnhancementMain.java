package com.nlp.sentiment.enhance;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Maps;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.nlp.sentiment.enhance.logic.LPSentimentEnhancer;
import com.nlp.sentiment.enhance.logic.LSISentimentEnhancer;
import com.nlp.sentiment.enhance.logic.SentimentEnhancer;
import com.nlp.sentiment.exception.LPInfluencerGraphBuilderException;
import com.nlp.sentiment.exception.SentimentEnhancerException;
import com.nlp.sentiment.exception.SentimentInfoFetcherException;
import com.nlp.sentiment.label.prop.graph.LPInfluencerGraphBuilder;
import com.nlp.sentiment.map.api.analytics.SentimentInfo;
import com.nlp.sentiment.metrics.CosineSimilarity;
import com.nlp.sentiment.type.TweetSentiment;
import com.sysomos.core.search.transfer.SearchResult;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.core.search.twitter.impl.GridTweetsSearchServiceImpl;
import com.sysomos.nlp.sentiment.Sentiment;
import com.sysomos.nlp.sentiment.SentimentEngine;

/**
 * 
 * @author adia
 *
 */
public class EnhancementMain {

	private static final Logger LOG = LoggerFactory
			.getLogger(EnhancementMain.class);
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String BASE_URL = "http://api.sysomos.com/v1/map/analytics/?";

	/**
	 * Enum values correspond to methods to use for labeling unlabeled
	 * documents.
	 * 
	 * @author adia
	 */
	private enum SentimentEnhancementMethod {
		LABEL_PROPAGATION(1) {
			@Override
			public Map<Integer, Sentiment> label(final SentimentResults sResults)
					throws SentimentEnhancerException {
				if (sResults == null) {
					return null;
				}
				List<String> labeled = sResults.getLabeledContent();
				List<String> unLabeled = sResults.getUnlabeledContent();
				List<String> content = sResults.getContent();

				double negProb = (1.0 * sResults.getNegListSize())
						/ content.size();
				double posProb = (1.0 * sResults.getPosListSize())
						/ content.size();
				double classProbs[][] = new double[labeled.size()][2];
				for (int i = 0; i < labeled.size(); i++) {
					classProbs[i] = new double[] { posProb, negProb };
				}

				LPSentimentEnhancer lp = new LPSentimentEnhancer(labeled,
						unLabeled, classProbs);
				return lp.label(sResults.getContentSentiments());
			}
		},
		LATENT_SEMANTIC_INDEXING(2) {
			@Override
			public Map<Integer, Sentiment> label(final SentimentResults sResults)
					throws SentimentEnhancerException {
				if (sResults == null) {
					return null;
				}

				List<String> labeled = sResults.getLabeledContent();
				List<String> unLabeled = sResults.getUnlabeledContent();
				List<Sentiment> labels = sResults.getContentSentiments();

				LSISentimentEnhancer lsi = new LSISentimentEnhancer(labeled,
						labels, unLabeled);
				Map<String, Sentiment> results = lsi.label(labels);

				int index = 0;
				Map<Integer, Sentiment> map = new HashMap<Integer, Sentiment>();
				for (Map.Entry<String, Sentiment> entry : results.entrySet()) {
					map.put(index++, entry.getValue());
				}
				return map;
			}
		};

		private SentimentEnhancementMethod(int method) {
			this.method = method;
		}

		private int method;

		public abstract Map<Integer, Sentiment> label(
				final SentimentResults sResults)
				throws SentimentEnhancerException;

		public static SentimentEnhancementMethod get(int method) {
			for (SentimentEnhancementMethod element : values()) {
				if (element.method == method) {
					return element;
				}
			}
			return null;
		}
	}

	/**
	 * Tells us which content to consider.
	 * 
	 * @author adia
	 */
	private enum SentimentInfoFetcher {

		TWEETS(1) {
			@Override
			public SentimentInfo getSentimentInfo(String xmlContent) {
				TweetSentiment twitterItem = new TweetSentiment(xmlContent);
				return twitterItem.getSentimentInfo();
			}
		},
		BLOGS(2) {
			@Override
			public SentimentInfo getSentimentInfo(String xmlContent) {
				return null;
			}
		},
		FORUMS(3) {
			@Override
			public SentimentInfo getSentimentInfo(String xmlContent) {
				return null;
			}

		},
		NEWS(4) {
			@Override
			public SentimentInfo getSentimentInfo(String xmlContent) {
				return null;
			}
		};

		private SentimentInfoFetcher(int contentType) {
			this.contentType = contentType;
		}

		private int contentType;

		public abstract SentimentInfo getSentimentInfo(String xmlContent);

		public static SentimentInfoFetcher get(int contentType) {
			for (SentimentInfoFetcher element : values()) {
				if (element.contentType == contentType) {
					return element;
				}
			}
			return null;
		}
	}

	/**
	 * TODO Change visibility
	 * 
	 * @param map
	 *            Map of user-sentiment
	 * @return a reorganized map of values where entries with positive
	 *         sentiments appear first, followed by entries with negative
	 *         sentiments, then entries with neutral or none sentiments.
	 */
	public Map<Long, Sentiment> reorganize(Map<Long, Sentiment> map) {
		if (MapUtils.isEmpty(map)) {
			return map;
		}
		Map<Long, Sentiment> sortedMap = new HashMap<Long, Sentiment>();
		Map<Long, Sentiment> negative = new HashMap<Long, Sentiment>();
		Map<Long, Sentiment> remaining = new HashMap<Long, Sentiment>();

		for (Map.Entry<Long, Sentiment> entry : map.entrySet()) {
			if (Sentiment.POS == entry.getValue()) {
				sortedMap.put(entry.getKey(), entry.getValue());
			} else if (Sentiment.NEG == entry.getValue()) {
				negative.put(entry.getKey(), entry.getValue());
			} else {
				remaining.put(entry.getKey(), entry.getValue());
			}
		}
		sortedMap.putAll(negative);
		sortedMap.putAll(remaining);

		return sortedMap;
	}

	/**
	 * Command line arguments
	 * 
	 * @author adia
	 */
	public static class Parameters {
		@Parameter(names = "-apiKey", required = false, description = "MAP API Key")
		public String apiKey;
		@Parameter(names = { "-query" }, required = true, description = "Specifies a query or a list "
				+ "of queries", variableArity = true)
		public List<String> queries;
		@Parameter(names = "-contentType", required = true, description = "Specifies which content to consider")
		public int contentType;
		@Parameter(names = "-method", required = true, description = "Method to use to assign sentiment to unlabeled contents")
		public int method;
		@Parameter(names = "-startDateMillis", required = true, description = "Start Date in milliseconds")
		public Long startDateMillis;
		@Parameter(names = "-endDateMillis", required = true, description = "End Date in milliseconds")
		public Long endDateMillis;
	}

	/**
	 * Output tweets matching a given search query.
	 * 
	 * @param query
	 *            Query
	 * @param apiKey
	 *            apiKey, needed for getting overall sentiment of a tweet
	 *            content using MAP Analytics API.
	 * @param contentType
	 *            One of (Tweet, Blog, Forum, News) @see
	 *            {@link SentimentInfoFetcher}
	 * @param startDateMillis
	 *            Start of the time window for searching the query
	 * @param endDateMillis
	 *            End of the time window for searching the query
	 * @return a list of TweetVO that match the search query
	 */
	public List<TweetVO> getMatchingTweets(String query, String apiKey,
			int contentType, long startDateMillis, long endDateMillis) {
		GridTweetsSearchServiceImpl service = new GridTweetsSearchServiceImpl();
		SearchResult<TweetVO> searchResult = service.search(query,
				startDateMillis, endDateMillis, 10000);

		return searchResult == null ? null : searchResult.getResults();

	}

	/**
	 * Based on a query and its matching tweets, this method organizes the
	 * results on a user-based.
	 * 
	 * @param query
	 * @param usersTweets
	 * @return
	 */
	public Map<Long, SentimentResults> getTweetSentiments(String query,
			final List<TweetVO> usersTweets) {

		if (CollectionUtils.isEmpty(usersTweets)) {
			return null;
		}

		Multimap<Long, String> usersPosMap = ArrayListMultimap.create();
		Multimap<Long, String> usersNegMap = ArrayListMultimap.create();
		Multimap<Long, String> usersNeutralMap = ArrayListMultimap.create();
		Multimap<Long, String> usersNoneMap = ArrayListMultimap.create();

		SentimentEngine sentimentEngine = new SentimentEngine(true);

		for (TweetVO tweet : usersTweets) {
			if (tweet == null || StringUtils.isEmpty(tweet.getContents())) {
				continue;
			}
			String content = tweet.getContents();
			Sentiment sentiment = Sentiment.NONE;
			sentiment = sentimentEngine.classify(content, true);
			if (sentiment == Sentiment.POS) {
				usersPosMap.put(tweet.getActorId(), content);
			} else if (sentiment == Sentiment.NEG) {
				usersNegMap.put(tweet.getActorId(), content);
			} else if (sentiment == Sentiment.NEUTRAL) {
				usersNeutralMap.put(tweet.getActorId(), content);
			} else {
				usersNoneMap.put(tweet.getActorId(), content);
			}
		}

		Set<Long> userIds = new HashSet<Long>(usersPosMap.keySet());
		userIds.addAll(usersNegMap.keySet());
		userIds.addAll(usersNeutralMap.keySet());

		Map<Long, SentimentResults> usersSentimentMap;
		usersSentimentMap = new HashMap<Long, SentimentResults>();
		for (Long userId : userIds) {
			SentimentResults results = new SentimentResults(query,
					new ArrayList<String>(usersPosMap.get(userId)),
					new ArrayList<String>(usersNegMap.get(userId)),
					new ArrayList<String>(usersNeutralMap.get(userId)),
					new ArrayList<String>(usersNoneMap.get(userId)));
			usersSentimentMap.put(userId, results);
		}

		return usersSentimentMap;
	}

	/**
	 * TODO: Remove this method after
	 * 
	 * @param params
	 *            Command line arguments that specify a collection of queries,
	 *            an apiKey, a start and end date (in milliseconds) @see
	 *            {@link Parameters}
	 * @return a set of {@link SentimentResults}.
	 * @throws SentimentInfoFetcherException
	 */
	public Set<SentimentResults> getSentiments(final Parameters args)
			throws SentimentInfoFetcherException {
		Set<SentimentResults> sentimentResultSet = new HashSet<SentimentResults>();

		for (String query : args.queries) {

			List<String> posList = new ArrayList<String>();
			List<String> negList = new ArrayList<String>();
			List<String> neutralList = new ArrayList<String>();
			List<String> noneList = new ArrayList<String>();

			List<TweetVO> docs = getMatchingTweets(query, args.apiKey,
					args.contentType, args.startDateMillis, args.endDateMillis);

			Map<Long, SentimentResults> usersSentimentsMap = getTweetSentiments(
					query, docs);
			if (MapUtils.isEmpty(usersSentimentsMap)) {
				continue;
			}
			for (Map.Entry<Long, SentimentResults> entry : usersSentimentsMap
					.entrySet()) {
				posList.addAll(entry.getValue().getPosList());
				negList.addAll(entry.getValue().getNegList());
				neutralList.addAll(entry.getValue().getNeutralList());
				noneList.addAll(entry.getValue().getNoneList());
			}

			SentimentResults results = new SentimentResults(query, posList,
					negList, neutralList, noneList);

			String xmlContent = getResultsXMLFormat(query, args.apiKey,
					args.startDateMillis, args.endDateMillis);
			SentimentInfoFetcher fetcher = SentimentInfoFetcher
					.get(args.contentType);

			if (fetcher == null) {
				String message = "Fetcher for content type"
						+ args.contentType
						+ " is null. Fetcher must be missing or not supported yet.";
				throw new SentimentInfoFetcherException(message);
			}

			SentimentInfo sentiment = fetcher.getSentimentInfo(xmlContent);

			if (sentiment == null) {
				continue;
			}

			float total = sentiment.getPositive() + sentiment.getNegative()
					+ sentiment.getNeutral();

			results.setPosSentimentProb(sentiment.getPositive() / total);
			results.setNegSentimentProb(sentiment.getNegative() / total);
			results.setNeutralSentimentProb(sentiment.getNeutral() / total);

			sentimentResultSet.add(results);

		}
		return sentimentResultSet;
	}

	/**
	 * Fetches the overall sentiment for a search query
	 * 
	 * @param query
	 *            Search keyword(s)
	 * @param apiKey
	 *            User MAP API key
	 * @param startDateMillis
	 *            Start of the time window to consider when searching contents
	 *            that match the query
	 * @param endDateMillis
	 *            End of the time to consider when searching contents that match
	 *            the query
	 * @return Some statistics in XML format
	 */
	public String getResultsXMLFormat(String query, String apiKey,
			long startDateMillis, long endDateMillis) {
		try {
			StringBuilder qb = new StringBuilder();
			qb.append(BASE_URL);
			qb.append("q=");
			qb.append(URLEncoder.encode(query, "UTF-8"));
			qb.append("&");
			qb.append("apiKey=");
			qb.append(apiKey);
			qb.append("&");
			qb.append("startDate=");
			qb.append(getStringDate(startDateMillis, DEFAULT_DATE_FORMAT));
			qb.append("&");
			qb.append("endDate=");
			qb.append(getStringDate(endDateMillis, DEFAULT_DATE_FORMAT));

			String queryUrl = new URI(qb.toString()).toString();

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet get = new HttpGet(queryUrl);
			HttpResponse responseGet = client.execute(get);
			HttpEntity resEntityGet = responseGet.getEntity();
			if (resEntityGet != null) {
				return EntityUtils.toString(resEntityGet);
			}
		} catch (ClientProtocolException e) {
			LOG.error("Query: " + query + " , Exception: {}",
					e.getLocalizedMessage(), e);
		} catch (URISyntaxException e) {
			LOG.error("Query: " + query + " , Exception: {}",
					e.getLocalizedMessage(), e);
		} catch (IOException e) {
			LOG.error("Query: " + query + " , Exception: {}",
					e.getLocalizedMessage(), e);
		} catch (ParseException e) {
			LOG.error("Query: " + query + " , Exception: {}",
					e.getLocalizedMessage(), e);
		}
		return null;
	}

	/**
	 * @param params
	 *            Command line arguments that specify a collection of queries,
	 *            an apiKey, a start and end date (in milliseconds) @see
	 *            {@link Parameters}
	 * @return a map of search query to some XML contents.
	 */
	public Map<String, String> getResultsXMLFormat(final Parameters args) {
		Map<String, String> queryResults = Maps.newHashMap();
		for (String query : args.queries) {
			String xmlContent = getResultsXMLFormat(query, args.apiKey,
					args.startDateMillis, args.endDateMillis);
			if (StringUtils.isEmpty(xmlContent)) {
				continue;
			}
			queryResults.put(query, xmlContent);
		}
		return queryResults;
	}

	private String getStringDate(long milliSeconds, String dateFormat) {
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	/**
	 * Method to call when attempting to label users and their contents that
	 * match the query.
	 * 
	 * @param query
	 *            Search query
	 * @param apiKey
	 *            MAP API key
	 * @param startDateMillis
	 *            Query start date in milliseconds
	 * @param endDateMillis
	 *            Query end date in milliseconds
	 * @param contentType
	 *            Specifies which content to consider. Content of type tweets
	 *            (1), blogs(2), forums(3), and news(4).
	 * @param method
	 *            Specifies which method to use. When method is 1, we use Label
	 *            Propagation, when method is 2 we use Latent Semantic Indexing.
	 * @return
	 * @throws SentimentEnhancerException
	 */
	public Map<Long, Sentiment> label(String query, String apiKey,
			long startDateMillis, long endDateMillis, int contentType,
			int method) throws SentimentEnhancerException {

		List<TweetVO> tweets = getMatchingTweets(query, apiKey, contentType,
				startDateMillis, endDateMillis);
		Map<Long, SentimentResults> userSentimentRsltMap = getTweetSentiments(
				query, tweets);

		Map<Long, Sentiment> userSentiments = new HashMap<Long, Sentiment>();
		for (Long userId : userSentimentRsltMap.keySet()) {
			SentimentResults sResults = userSentimentRsltMap.get(userId);
			Sentiment sentiment = labelUserTweets(sResults, method);
			userSentiments.put(userId, sentiment);
		}
		userSentiments = reorganize(userSentiments);
		double[][] probsDistr = classProbs(query, apiKey, startDateMillis,
				endDateMillis, contentType, method, userSentiments.size());
		labelUsers(userSentiments, tweets, probsDistr);
		return userSentiments;
	}

	/**
	 * Label tweets by applying either Latent Semantic Indexing or Label
	 * Propagation on a list of documents.
	 * 
	 * @param sResults
	 * @see {@link SentimentResults}
	 * @param method
	 *            Specifies which method to use. When method is 1, we use Label
	 *            Propagation, when method is 2 we use Latent Semantic Indexing.
	 * @return
	 * @throws SentimentEnhancerException
	 */
	public Sentiment labelUserTweets(SentimentResults sResults, int method)
			throws SentimentEnhancerException {

		List<String> posList = sResults.getPosList();
		List<String> negList = sResults.getNegList();
		List<String> unlabeled = sResults.getUnlabeledContent();

		if (CollectionUtils.isEmpty(unlabeled)) {
			return label(posList, negList);
		}

		Sentiment sentiment = Sentiment.NEUTRAL;
		SentimentEnhancementMethod sEMethod = SentimentEnhancementMethod
				.get(method);
		Map<Integer, Sentiment> unlabledNewLabels = sEMethod.label(sResults);

		if (MapUtils.isEmpty(unlabledNewLabels)) {
			return Sentiment.NONE;
		}

		for (Map.Entry<Integer, Sentiment> entry : unlabledNewLabels.entrySet()) {
			sentiment = entry.getValue();
			switch (sentiment) {
			case POS:
				posList.add(unlabeled.get(entry.getKey()));
			case NEG:
				negList.add(unlabeled.get(entry.getKey()));
			default:
				break;
			}
		}
		if (!(sentiment == Sentiment.NEUTRAL || sentiment == Sentiment.NONE)) {
			sentiment = label(posList, negList);
		}

		return sentiment;
	}

	/**
	 * Label users by applying the label propagation algorithm on top of the
	 * influencer graph built using some user IDS. TODO Change visibility
	 * 
	 * @param userSentiments
	 *            Map of user IDs to sentiments
	 * @param tweets
	 *            List of tweets
	 * @param probsDistr
	 *            Class label probability distribution
	 * @throws SentimentEnhancerException
	 */
	public void labelUsers(final Map<Long, Sentiment> userSentiments,
			final List<TweetVO> tweets, final double[][] probsDistr)
			throws SentimentEnhancerException {
		try {
			double[][] transWeights = LPInfluencerGraphBuilder.build(tweets,
					userSentiments);
			LPSentimentEnhancer lpEnhancer = new LPSentimentEnhancer(
					probsDistr, transWeights);

			List<Sentiment> labels = new ArrayList<Sentiment>();
			for (Map.Entry<Long, Sentiment> entry : userSentiments.entrySet()) {
				Sentiment s = entry.getValue();
				if (Sentiment.POS == s || Sentiment.NEG == s) {
					labels.add(s);
				}
			}
			Map<Integer, Sentiment> inferredLabels = lpEnhancer.label(labels);
			List<Long> ids = new ArrayList<Long>(userSentiments.keySet());
			for (Map.Entry<Integer, Sentiment> entry : inferredLabels
					.entrySet()) {
				long userID = ids.get(entry.getKey());
				userSentiments.put(userID, entry.getValue());
			}

		} catch (LPInfluencerGraphBuilderException e) {
			throw new SentimentEnhancerException(e.getLocalizedMessage());
		}
	}

	/**
	 * Summarizes results when all documents are known to have either a positive
	 * or negative sentiment.
	 * 
	 * @param posList
	 *            List of documents with a positive sentiment
	 * @param negList
	 *            List of documents with a negative sentiment.
	 * @return Sentiment
	 */
	private Sentiment label(final List<String> posList,
			final List<String> negList) {
		if (CollectionUtils.isEmpty(posList)) {
			if (!CollectionUtils.isEmpty(negList)) {
				return Sentiment.NEG;
			}
			return Sentiment.NONE;
		} else {
			if (CollectionUtils.isEmpty(negList)) {
				return Sentiment.POS;
			}
			if (posList.size() > negList.size()) {
				return Sentiment.POS;
			} else if (posList.size() < negList.size()) {
				return Sentiment.NEG;
			} else {
				return Sentiment.NEUTRAL;
			}
		}
	}

	/**
	 * Class labels probability distribution.
	 * 
	 * @param query
	 * @param apiKey
	 * @param startDateMillis
	 * @param endDateMillis
	 * @param contentType
	 * @param method
	 * @param size
	 * @return
	 * @throws SentimentEnhancerException
	 *             TODO Change visibility of method after.
	 */
	public double[][] classProbs(String query, String apiKey,
			long startDateMillis, long endDateMillis, int contentType,
			int method, int size) throws SentimentEnhancerException {

		String xmlContent = getResultsXMLFormat(query, apiKey, startDateMillis,
				endDateMillis);
		SentimentInfoFetcher fetcher = SentimentInfoFetcher.get(contentType);

		if (fetcher == null) {
			String message = "Fetcher for content type" + contentType
					+ " is null. Fetcher must be missing or not supported yet.";
			throw new SentimentEnhancerException(message);
		}
		SentimentInfo sentimentInfo = fetcher.getSentimentInfo(xmlContent);

		if (sentimentInfo == null) {
			throw new SentimentEnhancerException(
					"Couldn't create SentimentInfoFetcher from MAP API XML content.");
		}

		float total = sentimentInfo.getPositive() + sentimentInfo.getNegative()
				+ sentimentInfo.getNeutral();

		double posProb = (1.0 * sentimentInfo.getPositive()) / total;
		double negProb = (1.0 * sentimentInfo.getNegative()) / total;

		double classProbs[][] = new double[size][2];

		for (int i = 0; i < size; i++) {
			classProbs[i] = new double[] { posProb, negProb };
		}

		return classProbs;
	}

	/**
	 * Set to public only for testing purposes. TODO remove this method after as
	 * {@link SentimentEnhancer} already implements it.
	 * 
	 * @param documents
	 * @return
	 */
	public double[][] weight(final List<String> documents) {

		if (CollectionUtils.isEmpty(documents))
			return null;

		CosineSimilarity similarity = new CosineSimilarity();
		double weights[][] = new double[documents.size()][documents.size()];
		int indexI = 0;
		for (String docA : documents) {
			int indexJ = 0;
			double[] weightArr = new double[documents.size()];
			for (String docB : documents) {
				weightArr[indexJ++] = similarity.similarityScore(docA, docB);
			}
			weights[indexI++] = weightArr;
		}
		return weights;
	}
}
