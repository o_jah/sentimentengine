package com.nlp.sentiment.enhance;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.sysomos.nlp.sentiment.Sentiment;

public final class SentimentResults {

	private final String query;
	private final List<String> posList;
	private final List<String> negList;
	private final List<String> neutralList;
	private final List<String> noneList;
	private float posSentimentProb = 0.0f;
	private float negSentimentProb = 0.0f;
	private float neutralSentimentProb = 0.0f;

	/**
	 * Constructor
	 * 
	 * @param query
	 *            Query
	 * @param posList
	 *            List of contents of positive sentiments
	 * @param negList
	 *            List of contents of negative sentiments
	 * @param neutralList
	 *            List of contents of neutral sentiments
	 * @param noneList
	 *            List of contents with null sentiments
	 */
	public SentimentResults(String query, final List<String> posList,
			final List<String> negList, final List<String> neutralList,
			final List<String> noneList) {
		this.query = query;
		this.posList = posList;
		this.negList = negList;
		this.neutralList = neutralList;
		this.noneList = noneList;

		setProbabilities(CollectionUtils.isEmpty(posList) ? 0 : posList.size(),
				CollectionUtils.isEmpty(negList) ? 0 : negList.size(),
				CollectionUtils.isEmpty(neutralList) ? 0 : neutralList.size());

	}

	private void setProbabilities(float posSentiments, float negSentiments,
			float neutralSentiments) {
		float total = posSentiments + negSentiments + neutralSentiments;
		if (total == 0) {
			return;
		}
		this.posSentimentProb = posSentiments / total;
		this.negSentimentProb = negSentiments / total;
		this.neutralSentimentProb = neutralSentiments / total;
	}

	public String getQuery() {
		return query;
	}

	public List<String> getPosList() {
		return posList;
	}

	public List<String> getNegList() {
		return negList;
	}

	public int getNegListSize() {
		return CollectionUtils.isEmpty(negList) ? 0 : negList.size();
	}

	public int getPosListSize() {
		return CollectionUtils.isEmpty(posList) ? 0 : posList.size();
	}

	public List<String> getContent() {
		List<String> content = new ArrayList<String>(getLabeledContent());
		List<String> unlabeled = getUnlabeledContent();
		if (!CollectionUtils.isEmpty(unlabeled)) {
			content.addAll(unlabeled);
		}
		return content;
	}

	public Sentiment getSentimentFor(String doc) {
		if (contains(posList, doc)) {
			return Sentiment.POS;
		}
		if (contains(negList, doc)) {
			return Sentiment.NEG;
		}
		if (contains(neutralList, doc)) {
			return Sentiment.NEUTRAL;
		}
		return Sentiment.NONE;
	}

	public List<Sentiment> getContentSentiments() {
		if (CollectionUtils.isEmpty(posList)
				&& CollectionUtils.isEmpty(negList)) {
			return null;
		}
		List<Sentiment> labels = new ArrayList<Sentiment>();
		for (int i = 0; i < posList.size(); i++) {
			labels.add(Sentiment.POS);
		}
		for (int i = 0; i < negList.size(); i++) {
			labels.add(Sentiment.NEG);
		}
		return labels;
	}

	private boolean contains(List<String> documents, String doc) {
		if (CollectionUtils.isEmpty(documents)) {
			return false;
		}
		for (String aDoc : documents) {
			if (aDoc.equals(doc)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getLabeledContent() {
		List<String> labeled = new ArrayList<String>(posList);
		if (labeled != null && negList != null) {
			labeled.addAll(negList);
		}
		return labeled == null ? negList : labeled;
	}

	public List<String> getUnlabeledContent() {
		List<String> unLabeled = new ArrayList<String>(neutralList);
		if (unLabeled != null && noneList != null) {
			unLabeled.addAll(noneList);
		}
		return unLabeled == null ? noneList : unLabeled;
	}

	public List<String> getNoneList() {
		return noneList;
	}

	public float getPosSentimentProb() {
		return posSentimentProb;
	}

	public void setPosSentimentProb(float posSentimentProb) {
		this.posSentimentProb = posSentimentProb;
	}

	public float getNegSentimentProb() {
		return negSentimentProb;
	}

	public void setNegSentimentProb(float negSentimentProb) {
		this.negSentimentProb = negSentimentProb;
	}

	public float getNeutralSentimentProb() {
		return neutralSentimentProb;
	}

	public List<String> getNeutralList() {
		return neutralList;
	}

	public void setNeutralSentimentProb(float neutralSentimentProb) {
		this.neutralSentimentProb = neutralSentimentProb;
	}

	public float[] getClassProbs() {
		return new float[] { posSentimentProb, negSentimentProb };
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((negList == null) ? 0 : negList.hashCode());
		result = prime * result + Float.floatToIntBits(negSentimentProb);
		result = prime * result
				+ ((neutralList == null) ? 0 : neutralList.hashCode());
		result = prime * result + Float.floatToIntBits(neutralSentimentProb);
		result = prime * result
				+ ((noneList == null) ? 0 : noneList.hashCode());
		result = prime * result + ((posList == null) ? 0 : posList.hashCode());
		result = prime * result + Float.floatToIntBits(posSentimentProb);
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SentimentResults other = (SentimentResults) obj;
		if (negList == null) {
			if (other.negList != null)
				return false;
		} else if (!negList.equals(other.negList))
			return false;
		if (Float.floatToIntBits(negSentimentProb) != Float
				.floatToIntBits(other.negSentimentProb))
			return false;
		if (neutralList == null) {
			if (other.neutralList != null)
				return false;
		} else if (!neutralList.equals(other.neutralList))
			return false;
		if (Float.floatToIntBits(neutralSentimentProb) != Float
				.floatToIntBits(other.neutralSentimentProb))
			return false;
		if (noneList == null) {
			if (other.noneList != null)
				return false;
		} else if (!noneList.equals(other.noneList))
			return false;
		if (posList == null) {
			if (other.posList != null)
				return false;
		} else if (!posList.equals(other.posList))
			return false;
		if (Float.floatToIntBits(posSentimentProb) != Float
				.floatToIntBits(other.posSentimentProb))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}
}
