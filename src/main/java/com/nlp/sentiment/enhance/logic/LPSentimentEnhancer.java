package com.nlp.sentiment.enhance.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ejml.simple.SimpleMatrix;

import com.nlp.sentiment.exception.LPSentimentEnhancerException;
import com.nlp.sentiment.exception.SentimentEnhancerException;
import com.sysomos.nlp.sentiment.Sentiment;

public class LPSentimentEnhancer extends SentimentEnhancer<Integer> {

	private SimpleMatrix transitionMatrix = null;
	private SimpleMatrix labTransMatrix = null;
	private SimpleMatrix unLabTransMatrix = null;
	private SimpleMatrix classProbMatrix = null;

	public LPSentimentEnhancer(final double[][] classProbs,
			final double[][] transWeights) {
		super(transWeights);
	}

	/**
	 * Constructor
	 * 
	 * @param labeledDocs
	 *            List of documents with labels (Sentiment.POS, or
	 *            Sentiment.NEG).
	 * @param unLabeledDocs
	 *            List of unlabeled documents (contains neutrals and nones).
	 * @param classProbs
	 *            The probabilities of any data point being of any of the class
	 *            labels (sentiment).
	 * @throws LPSentimentEnhancerException
	 */
	public LPSentimentEnhancer(final List<String> labeledDocs,
			final List<String> unLabeledDocs, final double[][] classProbs)
			throws SentimentEnhancerException {
		super(labeledDocs, unLabeledDocs);
		if (classProbs == null) {
			throw new LPSentimentEnhancerException(
					"Class Probability values not provided.");
		}
		initialize(labeledDocs.size(), classProbs, weight());
	}

	private void initialize(int numLabInstances, final double[][] classProbs,
			final double[][] transWeights) throws LPSentimentEnhancerException {
		if (transWeights == null || transWeights.length <= numLabInstances
				|| classProbs == null || classProbs.length != numLabInstances) {
			throw new LPSentimentEnhancerException(
					"Encountered error when computing weights.");
		}
		transitionMatrix = new SimpleMatrix(
				rowNormalize(colNormalize(transWeights)));

		unLabTransMatrix = transitionMatrix.extractMatrix(numLabInstances,
				transWeights.length, numLabInstances, transWeights.length);
		labTransMatrix = transitionMatrix.extractMatrix(numLabInstances,
				transWeights.length, 0, numLabInstances);
		classProbMatrix = new SimpleMatrix(classProbs);
	}

	private double[][] rowNormalize(double[][] weights)
			throws LPSentimentEnhancerException {
		if (weights == null) {
			throw new LPSentimentEnhancerException(
					"Failed to normalize weights. Input array is null.");
		}
		for (int i = 0; i < weights.length; i++) {
			double sum = aggregate(weights[i]);
			if (sum == 0) {
				continue;
			}
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] /= sum;
			}
		}
		return weights;
	}

	private double[][] colNormalize(double[][] weights)
			throws LPSentimentEnhancerException {
		if (weights == null) {
			throw new LPSentimentEnhancerException(
					"Failed to normalize weights. Input array is null.");
		}
		int currRow = 0;
		while (true) {
			for (int j = 0; j < weights[currRow].length; j++) {
				double col[] = new double[weights.length];
				for (int i = 0; i < weights.length; i++) {
					col[i] = weights[i][j];
				}
				double sum = aggregate(col);
				if (sum == 0) {
					continue;
				}
				for (int i = 0; i < weights.length; i++) {
					weights[i][j] /= sum;
				}
			}
			currRow++;
			if (currRow >= weights.length) {
				return weights;
			}
		}
	}

	private double aggregate(final double[] weights)
			throws LPSentimentEnhancerException {
		if (weights == null) {
			throw new LPSentimentEnhancerException(
					"Failed to aggregate weights. Input array is null.");
		}
		double sum = 0;
		for (int i = 0; i < weights.length; i++) {
			sum += weights[i];
		}
		return sum;
	}

	@Override
	public Map<Integer, Sentiment> label(final List<Sentiment> sentiments)
			throws LPSentimentEnhancerException {
		SimpleMatrix fixpoint = computeFixpoint();

		if (fixpoint != null) {
			System.out.println("FP : [ rows: " + fixpoint.numRows()
					+ ", cols: " + fixpoint.numCols() + " ] ");
		}

		Map<Integer, Sentiment> inferredLabels = new HashMap<Integer, Sentiment>();
		for (int i = 0; i < unLabTransMatrix.numRows(); i++) {
			int maxProbIndex = 0;
			double maxProb = fixpoint.get(0, 0);
			for (int j = 0; j < fixpoint.numCols(); j++) {
				if (fixpoint.get(i, j) >= maxProb) {
					maxProb = fixpoint.get(i, j);
					maxProbIndex = j;
				}
			}
			if (maxProbIndex == 0) {
				inferredLabels.put(i, Sentiment.POS);
			} else {
				inferredLabels.put(i, Sentiment.NEG);
			}
		}
		return inferredLabels;
	}

	private SimpleMatrix computeFixpoint() throws LPSentimentEnhancerException {
		for (int i = 0; i < unLabTransMatrix.numRows(); i++) {
			unLabTransMatrix.set(i, i, 1 - unLabTransMatrix.get(i, i));
		}
		unLabTransMatrix = unLabTransMatrix.invert();
		if ((unLabTransMatrix.numCols() != labTransMatrix.numRows())
				|| (labTransMatrix.numCols() != classProbMatrix.numRows())) {
			System.out.println("UL: [ rows: " + unLabTransMatrix.numRows()
					+ ", cols: " + unLabTransMatrix.numCols() + " ]");
			System.out.println("LM: [ rows: " + labTransMatrix.numRows()
					+ ", cols: " + labTransMatrix.numCols() + " ]");
			System.out.println("YL: [ rows: " + classProbMatrix.numRows()
					+ ", cols: " + classProbMatrix.numCols() + " ]");
			throw new LPSentimentEnhancerException(
					"Failed to multiply matrices. Check Matrices dimensions.");
		}
		return unLabTransMatrix.mult(labTransMatrix).mult(classProbMatrix);
	}
}
