package com.nlp.sentiment.enhance.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.nlp.sentiment.exception.SentimentEnhancerException;
import com.nlp.sentiment.metrics.CosineSimilarity;
import com.sysomos.nlp.sentiment.Sentiment;

public abstract class SentimentEnhancer<T> {

	private final double[][] weights;
	protected final List<String> labeledDocs;
	protected final List<String> unLabeledDocs;

	public SentimentEnhancer(final double[][] weights) {
		this.weights = weights;
		this.labeledDocs = null;
		this.unLabeledDocs = null;
	}

	public SentimentEnhancer(final List<String> labeledDocs,
			final List<String> unLabeledDocs) throws SentimentEnhancerException {
		validate(labeledDocs, unLabeledDocs);
		this.weights = null;
		this.labeledDocs = labeledDocs;
		this.unLabeledDocs = unLabeledDocs;
	}

	private void validate(final List<String> labeledDocs,
			final List<String> unLabeledDocs) throws SentimentEnhancerException {
		if (CollectionUtils.isEmpty(labeledDocs)) {
			throw new SentimentEnhancerException(
					"List of labeled documents is empty or null.");
		}
		if (CollectionUtils.isEmpty(unLabeledDocs)) {
			throw new SentimentEnhancerException(
					"List of unlabeled documents is empty or null.");
		}
	}

	/**
	 * Method to call to get an unlabeled data point labeled.
	 * 
	 * @param sentiments
	 *            List of sentiments @see {@link SSentiment}. Defines the class
	 *            labels.
	 * @return a map that maps each unlabeled data point (here its index) to an
	 *         inferred sentiment
	 * @throws SentimentEnhancerException
	 */
	public abstract Map<T, Sentiment> label(List<Sentiment> sentiments)
			throws SentimentEnhancerException;

	protected final double[][] weight() {

		if (weights != null) {
			return weights;
		}

		List<String> documents = new ArrayList<String>(labeledDocs);
		if (!CollectionUtils.isEmpty(unLabeledDocs)) {
			documents.addAll(unLabeledDocs);
		}

		CosineSimilarity similarity = new CosineSimilarity();
		double weights[][] = new double[documents.size()][documents.size()];
		int indexI = 0;
		for (String docA : documents) {
			int indexJ = 0;
			double[] weightArr = new double[documents.size()];
			for (String docB : documents) {
				weightArr[indexJ++] = similarity.similarityScore(docA, docB);
			}
			weights[indexI++] = weightArr;
		}
		return weights;
	}
}
