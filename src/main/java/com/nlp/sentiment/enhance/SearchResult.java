package com.nlp.sentiment.enhance;

import java.util.List;

/**
 * 
 * @author JasonLiu
 *
 * @param <T>
 */
public class SearchResult<T> {

	private final String query;
	private final long totalResults;
	private final List<T> results;

	public SearchResult(String query, List<T> results, long totalResults) {
		this.query = query;
		this.totalResults = totalResults;
		this.results = results;
	}

	public List<T> getResults() {
		return results;
	}

	public long getTotalResults() {
		return totalResults;
	}

	public String getQuery() {
		return query;
	}
}
