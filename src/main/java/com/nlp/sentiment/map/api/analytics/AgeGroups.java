package com.nlp.sentiment.map.api.analytics;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "age_groups")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgeGroups {
	@XmlElement(name = "age_group")
	protected List<AgeGroup> ageGroups;

	public List<AgeGroup> getAgeGroups() {
		return ageGroups;
	}

	public void setAgeGroups(List<AgeGroup> ageGroups) {
		this.ageGroups = ageGroups;
	}

}