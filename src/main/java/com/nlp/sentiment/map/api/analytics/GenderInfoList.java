package com.nlp.sentiment.map.api.analytics;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "gender_demographics")
public class GenderInfoList {
	@XmlElement(name = "gender")
	protected List<GenderInfo> genderDemographics;

	public List<GenderInfo> getGenderDemographics() {
		return genderDemographics;
	}

	public void setGenderDemographics(List<GenderInfo> genderDemographics) {
		this.genderDemographics = genderDemographics;
	}
}