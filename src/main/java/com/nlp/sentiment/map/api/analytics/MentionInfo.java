package com.nlp.sentiment.map.api.analytics;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.nlp.sentiment.utils.Adapter1;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "mention")
public class MentionInfo {
	@XmlAttribute
	@XmlJavaTypeAdapter(Adapter1.class)
	protected Date date;
	@XmlAttribute
	protected Integer count;

	public MentionInfo() {
	}

	public MentionInfo(Date date, Integer count) {
		this.date = date;
		this.count = count;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
