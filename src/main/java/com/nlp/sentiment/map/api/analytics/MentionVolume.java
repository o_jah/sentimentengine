package com.nlp.sentiment.map.api.analytics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "volume")
@XmlAccessorType(XmlAccessType.FIELD)
public class MentionVolume {
	@XmlElement(name = "total_mentions")
	protected int totalMentions;
	@XmlElement(name = "mention_per_day")
	protected int mentionsPerDay;
	@XmlElement(name = "mention_per_hour")
	protected int mentionsPerHour;

	public int getTotalMentions() {
		return totalMentions;
	}

	public void setTotalMentions(int totalMentions) {
		this.totalMentions = totalMentions;
	}

	public int getMentionsPerDay() {
		return mentionsPerDay;
	}

	public void setMentionsPerDay(int tweetsPerDay) {
		this.mentionsPerDay = tweetsPerDay;
	}

	public int getMentionsPerHour() {
		return mentionsPerHour;
	}

	public void setMentionsPerHour(int tweetsPerHour) {
		this.mentionsPerHour = tweetsPerHour;
	}
}
