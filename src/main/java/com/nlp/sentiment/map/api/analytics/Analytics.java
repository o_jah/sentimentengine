package com.nlp.sentiment.map.api.analytics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "analytics")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "volume", "sentiment", "topCountries",
		"genderDemographics", "mentionTrend", "ageGroups" })
public class Analytics {
	@XmlElement(name = "volume")
	protected MentionVolume volume;
	@XmlElement(name = "top_countries")
	protected TopCountries topCountries;

	@XmlElement(name = "age_groups")
	protected AgeGroups ageGroups;

	@XmlElement(name = "gender_demographics")
	protected GenderInfoList genderDemographics;

	@XmlElement(name = "mention_trend")
	protected MentionTrend mentionTrend;

	@XmlElement(name = "overall_sentiment")
	protected SentimentInfo sentiment;

	public MentionVolume getVolume() {
		return volume;
	}

	public void setVolume(MentionVolume volume) {
		this.volume = volume;
	}

	public TopCountries getTopCountries() {
		return topCountries;
	}

	public void setTopCountries(TopCountries topCountries) {
		this.topCountries = topCountries;
	}

	public MentionTrend getMentionTrend() {
		return mentionTrend;
	}

	public void setMentionTrend(MentionTrend mentionTrend) {
		this.mentionTrend = mentionTrend;
	}

	public SentimentInfo getSentiment() {
		return sentiment;
	}

	public void setSentiment(SentimentInfo sentiment) {
		this.sentiment = sentiment;
	}

	public GenderInfoList getGenderDemographics() {
		return genderDemographics;
	}

	public void setGenderDemographics(GenderInfoList genderDemographics) {
		this.genderDemographics = genderDemographics;
	}

	public AgeGroups getAgeGroups() {
		return ageGroups;
	}

	public void setAgeGroups(AgeGroups ageGroups) {
		this.ageGroups = ageGroups;
	}

}
