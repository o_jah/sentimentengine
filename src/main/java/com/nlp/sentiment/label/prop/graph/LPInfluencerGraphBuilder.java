package com.nlp.sentiment.label.prop.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.mwired.grid.core.labs.influencer.InfluencerAlgorithm;
import com.mwired.grid.core.labs.influencer.graph.InfluencerEdge;
import com.mwired.grid.core.labs.influencer.graph.InfluencerGraph;
import com.mwired.grid.core.labs.transfer.Tweet;
import com.nlp.sentiment.exception.LPInfluencerGraphBuilderException;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.nlp.sentiment.Sentiment;

public class LPInfluencerGraphBuilder {

	public static double[][] build(final List<TweetVO> tweets,
			final Map<Long, Sentiment> sentiments)
			throws LPInfluencerGraphBuilderException {
		validate(tweets, sentiments);
		List<Tweet> adaptedTweets = adapt(tweets);
		InfluencerAlgorithm algorithm = new InfluencerAlgorithm(adaptedTweets);
		InfluencerGraph graph = algorithm.getInfluencerGraph();

		if (graph == null) {
			throw new LPInfluencerGraphBuilderException(
					"Couldn't get influencer graph from list of tweets.");
		}
		List<InfluencerEdge> edges = graph.getEdges();
		if (edges == null) {
			throw new LPInfluencerGraphBuilderException(
					"No edges in the graph. ");
		}

		double[][] weights = new double[sentiments.size()][sentiments.size()];

		for (int i = 0; i < weights.length; i++) {
			for (int j = i; j < weights.length; j++) {
				if (i == j) {
					weights[i][j] = 1;
				} else {
					weights[i][j] = 0;
					weights[j][i] = 0;
				}
			}
		}

		List<Long> ids = new ArrayList<Long>(sentiments.keySet());
		for (InfluencerEdge edge : edges) {
			if (sentiments.containsKey(edge.sourceID)
					&& sentiments.containsKey(edge.destinationID)) {
				int i = ids.indexOf(edge.sourceID);
				int j = ids.indexOf(edge.destinationID);
				weights[i][j] = edge.weight;
			}
		}
		return smooth(weights);
	}

	/**
	 * Add a Laplacian correction to the weights so that the propagation can
	 * work.
	 * 
	 * @param weights
	 * @return
	 */
	private static double[][] smooth(double[][] weights) {
		if (weights == null) {
			return weights;
		}

		for (int i = 0; i < weights.length; i++) {
			if (weights[i] == null)
				continue;
			double factor = 1.0 / weights[i].length;
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] += factor;
			}
		}
		return weights;
	}

	private static void validate(final List<TweetVO> tweets,
			final Map<Long, Sentiment> sentiments)
			throws LPInfluencerGraphBuilderException {
		if (MapUtils.isEmpty(sentiments) || CollectionUtils.isEmpty(tweets)) {
			throw new LPInfluencerGraphBuilderException("");
		}

		boolean unLabeledOnly = true, positiveOnly = true, negativeOnly = true;
		for (Map.Entry<Long, Sentiment> entry : sentiments.entrySet()) {
			positiveOnly = positiveOnly && (Sentiment.POS == entry.getValue());
			negativeOnly = negativeOnly && (Sentiment.NEG == entry.getValue());
			unLabeledOnly = unLabeledOnly
					&& (Sentiment.NEUTRAL == (entry.getValue()) || (Sentiment.NONE == entry
							.getValue()));
		}

		if (unLabeledOnly || positiveOnly || negativeOnly) {
			String cause = "Input map does not satisfy LP algorithm requirements. "
					+ "Map should contain positive, negative, and unlabeled instances.";
			throw new LPInfluencerGraphBuilderException(cause);
		}
	}

	private static List<Tweet> adapt(List<TweetVO> tweetsVO) {
		if (CollectionUtils.isEmpty(tweetsVO))
			return null;
		List<Tweet> adaptedTweets = new ArrayList<Tweet>();
		for (TweetVO aTweet : tweetsVO) {
			Tweet tw = new Tweet();
			tw.authorName = aTweet.getActorName();
			tw.createDate = aTweet.getCreateDate();
			tw.influenceScore = aTweet.getInfluenceScore();
			tw.screenName = aTweet.getActorScreenName();
			tw.text = aTweet.getContents();
			tw.tweetId = String.valueOf(aTweet.getDocId());
			tw.userIdString = String.valueOf(aTweet.getActorId());
			adaptedTweets.add(tw);
		}
		return adaptedTweets;
	}
}
