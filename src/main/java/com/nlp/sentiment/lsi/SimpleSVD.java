package com.nlp.sentiment.lsi;

import org.ejml.UtilEjml;
import org.ejml.alg.dense.decomposition.DecompositionFactory;
import org.ejml.alg.dense.decomposition.SingularValueDecomposition;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.SingularOps;
import org.ejml.simple.SimpleMatrix;

import com.nlp.sentiment.exception.LSIException;

public class SimpleSVD<T extends SimpleMatrix> {
	private SingularValueDecomposition<DenseMatrix64F> svd;
	private T U;
	private T W;
	private T V;

	private DenseMatrix64F matrix;

	public SimpleSVD(double matrix[][], boolean compact) throws LSIException {
		this(new DenseMatrix64F(matrix), false);
	}

	@SuppressWarnings("unchecked")
	public SimpleSVD(final DenseMatrix64F matrix, boolean compact)
			throws LSIException {
		this.matrix = matrix;
		svd = DecompositionFactory.svd(matrix.numRows, matrix.numCols, true,
				true, compact);
		if (!svd.decompose(matrix)) {
			throw new LSIException("Decomposition failed");
		}

		U = (T) SimpleMatrix.wrap(svd.getU(false));
		W = (T) SimpleMatrix.wrap(svd.getW(null));
		V = (T) SimpleMatrix.wrap(svd.getV(false));

		// order singular values from largest to smallest
		SingularOps.descendingOrder(U.getMatrix(), false, W.getMatrix(),
				V.getMatrix(), false);
	}

	/**
	 * <p>
	 * Returns the orthogonal 'U' matrix.
	 * </p>
	 *
	 * @return An orthogonal m by m matrix.
	 */
	public T getU() {
		return U;
	}

	/**
	 * Returns a diagonal matrix with the singular values. The singular values
	 * are ordered from largest to smallest.
	 *
	 * @return Diagonal matrix with singular values along the diagonal.
	 */
	public T getW() {
		return W;
	}

	/**
	 * <p>
	 * Returns the orthogonal 'V' matrix.
	 * </p>
	 *
	 * @return An orthogonal n by n matrix.
	 */
	public T getV() {
		return V;
	}

	/**
	 * <p>
	 * Computes the quality of the computed decomposition. A value close to or
	 * less than 1e-15 is considered to be within machine precision.
	 * </p>
	 *
	 * <p>
	 * This function must be called before the original matrix has been modified
	 * or else it will produce meaningless results.
	 * </p>
	 *
	 * @return Quality of the decomposition.
	 */
	public double quality() {
		return DecompositionFactory.quality(matrix, U.getMatrix(),
				W.getMatrix(), V.transpose().getMatrix());
	}

	/**
	 * Returns the specified singular value.
	 *
	 * @param index
	 *            Which singular value is to be returned.
	 * @return A singular value.
	 */
	public double getSingleValue(int index) {
		return W.get(index, index);
	}

	/**
	 * Returns the rank of the decomposed matrix.
	 *
	 * @see SingularOps#rank(org.ejml.factory.SingularValueDecomposition,
	 *      double)
	 *
	 * @return The matrix's rank
	 */
	public int rank() {
		return SingularOps.rank(svd, 10.0 * UtilEjml.EPS);
	}

	/**
	 * The nullity of the decomposed matrix.
	 *
	 * @see SingularOps#nullity(org.ejml.factory.SingularValueDecomposition,
	 *      double)
	 *
	 * @return The matrix's nullity
	 */
	public int nullity() {
		return SingularOps.nullity(svd, 10.0 * UtilEjml.EPS);
	}

	/**
	 * Returns the underlying decomposition that this is a wrapper around.
	 *
	 * @return SingularValueDecomposition
	 */
	public SingularValueDecomposition<DenseMatrix64F> getSVD() {
		return svd;
	}
}
