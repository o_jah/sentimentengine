package com.nlp.sentiment.exception;

public class LPSentimentEnhancerException extends SentimentEnhancerException {

	private static final long serialVersionUID = -134244424840017885L;

	public LPSentimentEnhancerException(Throwable cause) {
		super(cause);
	}

	public LPSentimentEnhancerException(String message) {
		super(message);
	}

	public LPSentimentEnhancerException(Exception e) {
		super(e);
	}
}
